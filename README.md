# Solve Story
Frederiko Adrian - 4210181024

Introduction
Solve Story adalah game dimana pemain harus harus melengkapi beberapa teks atau persamaan dengan sebuah huruf atau angka.
Pemain terdiri dari lebih dua pemain.
Akan disediakan sebuah teks narasi panjang yang ditampilkan kepada masing masing pemain.
Seluruh pemain harus dengan cepat melengkapi teks narasi yang dikosongi.
Setiap pemain dapat mengeluarkan jawabannya secara real-time.
Seluruh pemain harus dengan cermat mengamati keadaan yang ada di dalam teks tersebut supaya dapat melengkapi setiap teks yang dikosongi.
