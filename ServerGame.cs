using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace ServerDummy
{
    class ServerDummy
    {
        static readonly Dictionary<int, TcpClient> list_clients = new Dictionary<int, TcpClient>();

        static void Main(string[] args)
        {
            int count = 1;

            TcpListener ServerSocket = new TcpListener(IPAddress.Any, 8080);
            ServerSocket.Start();

            while (true)
            {
                Console.WriteLine("Menunggu pemain terhubung...");
                TcpClient client = ServerSocket.AcceptTcpClient();
                list_clients.Add(count, client);
                Console.WriteLine("Someone connected!!");

                Thread t;
                t = new Thread(handle_clients);
                t.Start(count);
                count++;
            }
        }

        public static void handle_clients(object o)
        {
            int id = (int)o;
            TcpClient client;

            client = list_clients[id];

            try
            {
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());
                string s = String.Empty;
                string username = String.Empty;

                bool bermain = false;
                bool hint = false;
                bool nomer1 = false;
                bool nomer2 = false;
                bool nomer3 = false;
                bool nomer4 = false;
                bool nomer5 = false;
                bool nomer6 = false;
                bool nomer7 = false;
                bool nomer8 = false;
                bool nomer9 = false;
                bool nomer10 = false;
                var timer = new Stopwatch();

                username = reader.ReadLine();
                Console.WriteLine(username + " telah terhubung!");
                broadcast(username + " join nih!");

                
                while (!(s = reader.ReadLine()).Equals("Exit") || (s == null))
                {
                    if (s == "main" && bermain == false)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine("Permainan telah dimulai!");
                        broadcast("Permainan telah dimulai!");

                        writer.WriteLine("There was a women lived in an apartment, she have a daugther lived with her. One day, she tell her mother that she was going out to play with her friends. A few hours later, when the mother went to the room to call her ___ in for dinner, she realize that her daughter wasn't there.");
                        writer.Flush();
                        bermain = true;
                        hint = true;
                        nomer1 = true;
                        timer.Start();
                    }
                    else if (s == "hint" && hint == true)
                    {
                        writer.WriteLine("[This is the list of the answer, there are question that you can answer with more than one possibility answer!]");
                        writer.WriteLine("[s _ _ a _ c _]		[_ a t _ _]	[_a_k]	[_ a _ g _ _ e _]	[_ l _ _ b e _]");
                        writer.WriteLine("[_ l _ _ t _ _ g]	[p _ _ i _ e]	[f _ l _ _ e _]	[g _ n e]	[c _ _ p _ e]");
                        writer.WriteLine("[j _ _ i _ o _]");
                        writer.Flush();
                    }
                    else if (s == "daughter" && nomer1 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("Then, the mother asked all of the kids who lived in the apartment if they had seen her daughter. All of the kids told that they didn't look her daughter all day long. Then she realized that her daughter was completely ___.");
                        writer.Flush();
                        nomer1 = false;
                        nomer2 = true;
                    }
                    else if (s == "gone" && nomer2 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("She called the ___ and search of the surrounding area was held.");
                        writer.Flush();
                        nomer2 = false;
                        nomer3 = true;
                    }
                    else if (s == "police" && nomer3 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("All of the other parents in the apartment joined the ___, but it does not make any changes. In the end the girl was never found.");
                        writer.Flush();
                        nomer3 = false;
                        nomer4 = true;
                    }
                    else if (s == "search" && nomer4 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("Several weeks later, the residents of the apartment complex began to complain about their drinking water supply. Every time they turned on the tap in their apartment, they saw a strange smell from that ___.");
                        writer.Flush();
                        nomer4 = false;
                        nomer5 = true;
                    }
                    else if (s ==  "water" && nomer5 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("The apartment managers decided that something must be done and told the ___ to test the water in the roof storage tank.");
                        writer.Flush();
                        nomer5 = false;
                        nomer6 = true;
                    }
                    else if (s == "janitor" || s == "plumber" && nomer6 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("The janitor/plumber climbs to the roof and starts removing the lid from the tank and checking the chemical content of the water. When he got to the last ___, he lifted the lid and hit it with a strong smell.");
                        writer.Flush();
                        nomer6 = false;
                        nomer7 = true;
                    }
                    else if (s == "tank" && nomer7 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("Staring into the gloomy depths, he could see something ___ in the water. It is the body of a decaying child.");
                        writer.Flush();
                        nomer7 = false;
                        nomer8 = true;
                    }
                    else if (s == "floating" && nomer8 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("After the police conducted an autopsy, they could confirm that it was the ___ of the little girl who had disappeared three months earlier.");
                        writer.Flush();
                        nomer8 = false;
                        nomer9 = true;
                    }
                    else if (s == "corpse" && nomer9 == true)
                    {
                        Console.WriteLine(username + " : " + s);
                        Console.WriteLine(username + " have answered correctly!");
                        broadcast(username + " have answered correctly!");
                        writer.WriteLine("It seems he has been playing on the roof when the temporary water storage tank cap is removed. Apparently, he looked into the tank and must have accidentally ___ into the water and drowned.");
                        writer.Flush();
                        nomer9 = false;
                        nomer10 = true;
                    }
                    else if (s == "fallen" && nomer10 == true)
                    {
                        timer.Stop();
                        writer.WriteLine("The body of the girl lay there for three full months. And for three full months, the residents of the apartment unknowingly drank water whose body was rotting a floating girl.");
                        writer.Flush();
                        broadcast(username + " telah menyelesaikan cerita dengan waktu : " + timer.Elapsed.ToString());
                        broadcast("Pemenangnya adalah " + username + "!");
                        nomer10 = false;
                        bermain = false;
                    }
                    else
                    {
                        Console.WriteLine(username + " : " + s);
                        broadcast(username + " : " + s);
                    }
                }
                list_clients.Remove(id);
                client.Client.Shutdown(SocketShutdown.Both);
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Memutuskan koneksi.");
            }
            catch (IOException)
            {
                Console.WriteLine("Menutup thread.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }

        public static void broadcast(string data)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(data + Environment.NewLine);

            {
                foreach (TcpClient c in list_clients.Values)
                {
                    NetworkStream stream = c.GetStream();
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
        }
    }
}