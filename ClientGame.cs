using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace ClentGame
{
    class Program
    {
        public static bool bermain = false;
        public static TcpClient client;
        public static NetworkStream n;

        static void Main(string[] args)
        {
            Console.WriteLine("|===============================|");
            Console.WriteLine("|===[Welcome to Solve Story!]===|");
            Console.WriteLine("|===============================|");
            Console.WriteLine("|=1. Mulai bermain dengan teman=|");
            Console.WriteLine("|========2. Cara Bermain========|\n");
            Console.Write("[Pilih Menu] : ");
            string inputMenu = Console.ReadLine();
            if (inputMenu == "1")
            {
                game();
            }
            else if (inputMenu == "2")
            {
                howtoplay(args);
            }
            else
            {
                Main(args);
            }
        }
        public static void howtoplay(string[] args)
        {
            Console.WriteLine("1.Kamu akan disajikan beberapa kalimat rumpang yang merupakan bagian-bagian dari suatu cerita yang utuh");
            Console.WriteLine("2. Kamu harus melengkapi kalimat rumpang tersebut supaya dapat menyelesaikan cerita tersebut");
            Console.WriteLine("3. Kamu harus menyelesaikan game dengan cepat karena kamu akan bersaing dengan temanmu yang lain");
            Console.WriteLine("Selamat Bermain! :D");
            Main(args);
        }

        public static void game()
        {
            Console.WriteLine("[Masukkan IP Server...]");
            string ServerIP = Console.ReadLine();
            client = new TcpClient(ServerIP, 8080);
            n = client.GetStream();
            Console.WriteLine("[Terhubung]");
            StreamReader o = new StreamReader(n);
            StreamWriter p = new StreamWriter(n);

            string client_name;
            string server_name;
            string pesan_client = "";

            //username
            Console.Write("[Masukkan username] : ");
            client_name = Console.ReadLine();
            p.WriteLine(client_name);
            p.Flush();

            server_name = o.ReadLine();
            Console.WriteLine(server_name);
            Console.WriteLine("[Tulis 'main' untuk membuka soal!]");
            Console.WriteLine("[Tulis 'hint' untuk mendapatkan petunjuk saat bermain!]");
            Console.WriteLine("[Jangan lupa untuk menjawab dengan lowercase!]\n");

            Thread thread = new Thread(g => ReceiveData((TcpClient)g));

            thread.Start(client);

            while (true)
            {// mengirim data ke server 
                if (pesan_client != "Exit")
                {
                    //Console.WriteLine("[Tulis Pesan] : ");
                    pesan_client = Console.ReadLine();
                    p.WriteLine(pesan_client);
                    p.Flush();
                    if (pesan_client == "main" && bermain == false)
                    {
                        bermain = true;
                    }
                }
            }
        }
        static void ReceiveData(TcpClient client)
        {
            NetworkStream ns = client.GetStream();
            StreamWriter p = new StreamWriter(n);
            byte[] receivedBytes = new byte[1024];

            int byte_count;

            while ((byte_count = ns.Read(receivedBytes, 0, receivedBytes.Length)) > 0)
            {
                Console.Write(Encoding.ASCII.GetString(receivedBytes, 0, byte_count));
                string pesan_server = Encoding.ASCII.GetString(receivedBytes, 0, byte_count);
                if (pesan_server == "Permainan telah dimulai!" + Environment.NewLine && bermain == false)
                {
                    bermain = true;
                    p.WriteLine("main");
                    p.Flush();
                }
            }
        }
    }
}